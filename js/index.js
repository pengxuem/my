
//调用全屏的方法
$(function(){
    $('#dowebok').fullpage({
    	// 设置每一屏的颜色
    	sectionsColor:["#0da5b6","#2AB561","#DE8910","#16BA9D","#0DA5D6"],
    	// 滚动到某一屏幕之后调用
    	afterLoad:function(link,index){
    		
    		// index  当前section的编号
    		// current加给谁谁就做动画
    		$(".section").removeClass("current");
    		// 添加定时器让动画延迟执行 延迟100ms
    		setTimeout(function(){
    			$(".section").eq(index-1).addClass("current");
    		},100);
    	}
    });
});